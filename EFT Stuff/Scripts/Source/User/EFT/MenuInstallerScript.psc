Scriptname EFT:MenuInstallerScript extends Quest

Group EFTOtherValuesCache
    Holotape Property Script_EFTHoloTape Auto Const Mandatory
    {Reference to Script_EFTHoloTape Holotape}
    GlobalVariable Property script_EFTMenuInstalled Auto
    {Reference to script_EFTMenuInstalled Global}
EndGroup

Group EFTVanillaWorkshopMenuCache
    FormList Property WorkshopMenuMain auto const
    {Add to the WorkshopMenuMain - Main Menu}
    FormList Property WorkshopMenu01Build auto const
    {Add to the WorkshopMenu01Build - Structures}
    FormList Property WorkshopMenu01Furniture auto const
    {Add to the WorkshopMenu01Furniture - Furniture}
    FormList Property WorkshopMenu01Decor auto const
    {Add to the WorkshopMenu01Decor - Decorations}
    FormList Property WorkshopMenu01Power auto const
    {Add to the WorkshopMenu01Power - Power}
    FormList Property WorkshopMenu01Resource02Defense auto const
    {Add to the WorkshopMenu01Resource02Defense - Defense}
    FormList Property WorkshopMenu01Resource auto const
    {Add to the WorkshopMenu01Resource - Resource Production}
    FormList Property WorkshopMenu01Vendors auto const
    {Add to the WorkshopMenu01Vendors - Vendors}
    FormList Property WorkshopMenu01Cages auto const
    {Add to the WorkshopMenu01Cages - Cages}
    FormList Property WorkshopMenu01Raider auto const
    {Add to the WorkshopMenu01Raider - Raider}
EndGroup

Group EFTCustomWorkshopMenuCache
    FormList Property EFT_CustomMainMenu_Form auto const
    {Add the EFT_CustomMainMenu_Form for Single Menu}

    FormList Property EFT_Structures_MultiForm auto const
    {Add the EFT_Structures_MultiForm}
    FormList Property EFT_Furniture_MultiForm auto const
    {Add the EFT_Furniture_MultiForm}
    FormList Property EFT_Decorations_MultiForm auto const
    {Add the EFT_Decorations_MultiForm}
    FormList Property EFT_Decorations_OCDecorator_Form auto const
    {Add the EFT_Decorations_OCDecorator_Form}
    FormList Property EFT_Electricity_MultiForm auto const
    {Add the EFT_Electricity_MultiForm}
    FormList Property EFT_Defense_MultiForm auto const
    {Add the EFT_Defense_MultiForm}
    FormList Property EFT_Resources_MultiForm auto const
    {Add the EFT_Resources_MultiForm}
    FormList Property EFT_Vendors_MultiForm auto const
    {Add the EFT_Vendors_MultiForm}
    FormList Property EFT_Workbenches_MultiForm auto const
    {Add the EFT_Workbenches_MultiForm}
    FormList Property EFT_Cages_MultiForm auto const
    {Add the EFT_Cages_MultiForm}
    FormList Property EFT_Landscaping_MultiForm auto const
    {Add the EFT_Landscaping_MultiForm}
    FormList Property EFT_SettlerManagement_Form auto const
    {Add the EFT_SettlerManagement_Form}
    FormList Property EFT_MainHomemakerMenu_Form auto const
    {Add the EFT_MainHomemakerMenu_Form}
    FormList Property EFT_DDProductions_Form auto const
    {Add the EFT_DDProductions_Form}
    FormList Property EFT_CustomScript_Form auto const
    {Add the EFT_CustomScript_Form}
    FormList Property EFT_ExtraSettlementItems_Form auto const
    {Add the EFT_ExtraSettlementItems_Form}
EndGroup

;TODO - remove if you dont want to use a while loop
bool uninstallCheckRunning = false
bool usingMultiMenu = false

;This bool will be added to save game and used with the plugin's glogal for the Holotape Menu
bool menuInstalled = true

;====================================================================================================================
; Quest and Register Events
;====================================================================================================================

Event OnQuestInit()
    Game.GetPlayer().AddItem(Script_EFTHoloTape, 1)
    install_menu()
    RegisterForRemoteEvent(Game.GetPlayer(), "OnPlayerLoadGame")
    ;TODO - remove if you dont want to use a while loop
    checkForUninstall()
    RegisterForRemoteEvent(Game.GetPlayer(), "OnDifficultyChanged")
EndEvent

;====================================================================================================================
; OnPlayerLoadGame and OnDifficultyChanged
;====================================================================================================================

Event Actor.OnPlayerLoadGame(Actor actorref)
    setGlobalFromProperty()
    install_menu()
    ;TODO - remove if you dont want to use a while loop
    checkForUninstall()
EndEvent

Event Actor.OnDifficultyChanged(Actor actorref, int aOldDifficulty, int aNewDifficulty)
    setGlobalFromProperty()
    install_menu()
    ;TODO - remove if you dont want to use a while loop
    checkForUninstall()
EndEvent

;====================================================================================================================
; Set Plugin Global for Holotape and Save Game
;====================================================================================================================

Function setGlobalTrue()
    menuInstalled = true
    script_EFTMenuInstalled.SetValueInt(1)
EndFunction

Function setGlobalFalse()
    menuInstalled = false
    script_EFTMenuInstalled.SetValueInt(0)
EndFunction

Function setGlobalFromProperty()
    if (!menuInstalled)
        setGlobalFalse()
    else
        setGlobalTrue()
    endif
EndFunction

;====================================================================================================================
; Install and Uninstall deciders
;====================================================================================================================

Function install_menu()
    if (!usingMultiMenu)
        single_install()
    else
        multimenu_install()
    endif
EndFunction

Function uninstall_menu()
    if (!usingMultiMenu)
        single_uninstall()
    else
        multimenu_uninstall()
    endif
EndFunction

;====================================================================================================================
; Single Menu Install and Restore
;====================================================================================================================

Function single_install()
    ; Change to false for Single Menu Install
    usingMultiMenu = false

    WorkshopMenuMain.addForm(EFT_CustomMainMenu_Form)
    setGlobalTrue()
EndFunction

Function single_uninstall()
    WorkshopMenuMain.RemoveAddedForm(EFT_CustomMainMenu_Form)
    setGlobalFalse()
EndFunction

;====================================================================================================================
; Multi Menu Install and Restore
;====================================================================================================================

;This function adds your new menus or categories to the vanilla formlists
Function multimenu_install()
    ; Change to True for Multi Menu Install
    usingMultiMenu = true

    WorkshopMenu01Build.addForm(EFT_Structures_MultiForm)
    WorkshopMenu01Furniture.addForm(EFT_Furniture_MultiForm)
    WorkshopMenu01Decor.addForm(EFT_Decorations_MultiForm)
    WorkshopMenu01Decor.addForm(EFT_Decorations_OCDecorator_Form)
    WorkshopMenu01Power.addForm(EFT_Electricity_MultiForm)
    WorkshopMenu01Resource02Defense.addForm(EFT_Defense_MultiForm)
    WorkshopMenu01Resource.addForm(EFT_Resources_MultiForm)
    WorkshopMenu01Vendors.addForm(EFT_Vendors_MultiForm)
    WorkshopMenuMain.addForm(EFT_Workbenches_MultiForm)
    WorkshopMenu01Cages.addForm(EFT_Cages_MultiForm)
    WorkshopMenu01Decor.addForm(EFT_Landscaping_MultiForm)
    WorkshopMenuMain.addForm(EFT_SettlerManagement_Form)
    WorkshopMenuMain.addForm(EFT_MainHomemakerMenu_Form)
    WorkshopMenuMain.addForm(EFT_DDProductions_Form)
    WorkshopMenuMain.addForm(EFT_CustomScript_Form)
    WorkshopMenuMain.addForm(EFT_ExtraSettlementItems_Form)

    setGlobalTrue()
EndFunction

Function multimenu_uninstall()
    WorkshopMenu01Build.RemoveAddedForm(EFT_Structures_MultiForm)
    WorkshopMenu01Furniture.RemoveAddedForm(EFT_Furniture_MultiForm)
    WorkshopMenu01Decor.RemoveAddedForm(EFT_Decorations_MultiForm)
    WorkshopMenu01Decor.RemoveAddedForm(EFT_Decorations_OCDecorator_Form)
    WorkshopMenu01Power.RemoveAddedForm(EFT_Electricity_MultiForm)
    WorkshopMenu01Resource02Defense.RemoveAddedForm(EFT_Defense_MultiForm)
    WorkshopMenu01Resource.RemoveAddedForm(EFT_Resources_MultiForm)
    WorkshopMenu01Vendors.RemoveAddedForm(EFT_Vendors_MultiForm)
    WorkshopMenuMain.RemoveAddedForm(EFT_Workbenches_MultiForm)
    WorkshopMenu01Cages.RemoveAddedForm(EFT_Cages_MultiForm)
    WorkshopMenu01Decor.RemoveAddedForm(EFT_Landscaping_MultiForm)
    WorkshopMenuMain.RemoveAddedForm(EFT_SettlerManagement_Form)
    WorkshopMenuMain.RemoveAddedForm(EFT_MainHomemakerMenu_Form)
    WorkshopMenuMain.RemoveAddedForm(EFT_DDProductions_Form)
    WorkshopMenuMain.RemoveAddedForm(EFT_CustomScript_Form)
    WorkshopMenuMain.RemoveAddedForm(EFT_ExtraSettlementItems_Form)

    setGlobalFalse()
EndFunction

;====================================================================================================================
; Check For Uninstall
;====================================================================================================================

;Checks if your plugin is still active. If not removes every none value from the vanilla formlists.
Function checkForUninstall()
    ;we only want the loop once
    if (!uninstallCheckRunning)
        uninstallCheckRunning = true
        ;TODO - replace YourMod.esp with the name of your esp
        while (Game.IsPluginInstalled("SettlementKeywords.esm"))
            Utility.wait(60) ;waits 60 seconds until the condition is checked again
        endwhile
        ;The plugin is no longer active. Now we have to remove every none value from the edited formlists.

        ;TODO - Do this with every vanilla menu you edited
        removeNoneValues(WorkshopMenuMain)
        removeNoneValues(WorkshopMenu01Build)
        removeNoneValues(WorkshopMenu01Furniture)
        removeNoneValues(WorkshopMenu01Decor)
        removeNoneValues(WorkshopMenu01Power)
        removeNoneValues(WorkshopMenu01Resource02Defense)
        removeNoneValues(WorkshopMenu01Resource)
        removeNoneValues(WorkshopMenu01Vendors)
        removeNoneValues(WorkshopMenu01Cages)
        ;...
        uninstallCheckRunning = false
    endif
EndFunction

;====================================================================================================================
; HoloTape Menu Fixer
;====================================================================================================================

;Run Menu Fixer by request from HoloTape
Function holoTapeMenuFixer()
    ;we only want the loop once
    if (!uninstallCheckRunning)
        uninstallCheckRunning = true

        removeNoneValues(WorkshopMenuMain)
        removeNoneValues(WorkshopMenu01Build)
        removeNoneValues(WorkshopMenu01Furniture)
        removeNoneValues(WorkshopMenu01Decor)
        removeNoneValues(WorkshopMenu01Power)
        removeNoneValues(WorkshopMenu01Resource02Defense)
        removeNoneValues(WorkshopMenu01Resource)
        removeNoneValues(WorkshopMenu01Vendors)
        removeNoneValues(WorkshopMenu01Cages)

        uninstallCheckRunning = false
    endif
    Debug.Notification("Settlement Keywords Menu Fixer Finished!")
EndFunction

;====================================================================================================================
; Dump Menus to Log
;====================================================================================================================

;Run Menu dump on all altered menus from HoloTape
Function holoTapeMenuDump()
    displayAllValues(WorkshopMenuMain)
    displayAllValues(WorkshopMenu01Build)
    displayAllValues(WorkshopMenu01Furniture)
    displayAllValues(WorkshopMenu01Decor)
    displayAllValues(WorkshopMenu01Power)
    displayAllValues(WorkshopMenu01Resource02Defense)
    displayAllValues(WorkshopMenu01Resource)
    displayAllValues(WorkshopMenu01Vendors)
    displayAllValues(WorkshopMenu01Cages)
EndFunction

Function displayAllValues(Formlist akCurList)
    Int iSize = akCurList.GetSize()
    ;Check the size of akCurList to see if it has anything in it
    If iSize > 0

        Int i
        Form[] tmp = New Form[iSize]
        While i < iSize
            tmp[i] = akCurList.GetAt(i)
            Debug.Trace("Value " + i + ": " + (tmp[i] as string))
            i += 1
        EndWhile

    EndIf
EndFunction

;====================================================================================================================
; Remove invalid Forms
;====================================================================================================================

Function removeNoneValues(Formlist akCurList)
    Int iSize = akCurList.GetSize()
    ;Check the size of akCurList to see if it has anything in it
    If iSize > 0

        ;Copy the Form List
        Int i
        Form[] tmp = New Form[iSize]
        While i < iSize
            tmp[i] = akCurList.GetAt(i)
            i += 1
        EndWhile

        ;Revert the Form List
        akCurList.Revert()

        ;Restore the Form List
        i = 0
        While i < iSize
            If tmp[i] != None
                akCurList.AddForm(tmp[i])
            EndIf
            i += 1
        EndWhile
        tmp.Clear()
    EndIf
EndFunction