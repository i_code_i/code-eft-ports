Scriptname BarbedWireTrigger extends ObjectReference
{  }

;-- Properties --------------------------------------
Group Main
	Spell Property BarbedWireTouch auto
	Sound Property BarbedWireTouchSound auto
EndGroup


;-- Variables ---------------------------------------
	Actor Target
	InputEnableLayer inputLayer

;-- Functions ---------------------------------------

Event OnTriggerEnter(ObjectReference akActionRef)
	
	RegisterForAnimationEvent(akActionRef, "FootLeft")
	RegisterForAnimationEvent(akActionRef, "FootRight")
	RegisterForAnimationEvent(akActionRef, "FootSprintLeft")
	RegisterForAnimationEvent(akActionRef, "FootSprintRight")
	if akActionRef == Game.GetPlayer()
		inputLayer = InputEnableLayer.Create()
		Target = Game.GetPlayer()
		inputLayer.EnableRunning(false)
		inputLayer.EnableSprinting(false)
	else
		Target = akActionRef as Actor
	endif
	
EndEvent

Event OnAnimationEvent(ObjectReference akSource, string asEventName)
	if (asEventName == "FootLeft" || asEventName == "FootRight" || asEventName == "FootSprintLeft" || asEventName == "FootSprintRight")
		BarbedWireTouch.Cast(self, Target)
		BarbedWireTouchSound.Play(self)
	endif
EndEvent

Event onTriggerLeave(ObjectReference akActionRef)
	inputLayer.EnableRunning(true)
	inputLayer.EnableSprinting(true)
	self.InterruptCast()
	UnregisterForAnimationEvent(akActionRef, "FootLeft")
	UnregisterForAnimationEvent(akActionRef, "FootRight")
	UnregisterForAnimationEvent(akActionRef, "FootSprintLeft")
	UnregisterForAnimationEvent(akActionRef, "FootSprintRight")
	
EndEvent